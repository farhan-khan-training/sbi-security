var keycloak = new Keycloak();

function initKeycloak() {
  keycloak
    .init({
      onLoad: "login-required",
      redirectUri:
        "https://uatapp.syber1-dev.com/#/container-v1/attack-simulation-v1/attack-vector",
    })
    .then(function () {
      console.log("token", keycloak.idTokenParsed);
      printData(keycloak.idTokenParsed);
    })
    .catch(function (err) {
      console.log("error", err);
      alert("failed to initialize");
    });
}

function printData(keycloakToken) {
  console.log("printData", keycloakToken.preferred_username);
  document.getElementById("login").innerHTML = keycloakToken.preferred_username;
}

var logout = function () {};
