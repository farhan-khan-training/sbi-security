$(".icon-box").click(function () {
  if (!$(this).hasClass("open-box")) {
    $(".icon-box").removeClass("open-box");
    $(this).addClass("open-box");
  } else {
    $(".icon-box").removeClass("open-box");
  }
});

$(".know-more").click(function () {
  if (!$(this).parent().parent().hasClass("open-more")) {
    $(this).children(".show-more").hide();
    $(this).children(".show-less").show();
    $(".know-more").parent().parent().removeClass("open-more");
    $(this).parent().parent().addClass("open-more");
  } else {
    $(this).children(".show-more").show();
    $(this).children(".show-less").hide();
    $(".know-more").parent().parent().removeClass("open-more");
  }
});
