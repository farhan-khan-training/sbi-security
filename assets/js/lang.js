// show english language on load
$(function () {
  $('[lang="en"]').hide();
  $('[lang="jp"]').show();
});

$(document).ready(function () {
  // toggle language selector drop down
  $(".selected-box").on("click", function () {
    let component = $(this).parent().children(".lang-drop-down");

    if (component.hasClass("show")) {
      component.removeClass("show");
    } else {
      component.addClass("show");
    }
  });

  $(".custom-lang-drop-down").bind("blur", function () {
    let component = $(this).children(".lang-drop-down");
    if (component.hasClass("show")) {
      component.removeClass("show");
    }
  });

  // select language option
  $(".langSelector .option").on("click", function () {
    $(".langSelector").children().removeClass("selected");
    if (!$(this).hasClass("selected")) {
      $(this).addClass("selected");
    }

    if ($(this).hasClass("en")) {
      $('[lang="en"]').show();
      $('[lang="jp"]').hide();
    } else {
      $('[lang="en"]').hide();
      $('[lang="jp"]').show();
    }

    if ($(this).parent().hasClass("show")) {
      $(this).parent().removeClass("show");
    }
  });
});
